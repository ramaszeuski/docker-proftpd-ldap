FROM alpine:latest

RUN apk update && apk add \
    proftpd \
    proftpd-mod_ldap

COPY proftpd.conf /etc/proftpd/proftpd.conf

EXPOSE 20 21

CMD ["proftpd", "-n" ]
